4 种编码类型

1. 字符串：<字符串长度>:<字符串>，如announce，编码后为8:announce
2. 整型：i<十进制整型数>e，如9，编码后为i9e
3. 列表：l<任何合法类型>e
4. 字典：d<关键字><值>e，其中，关键字是一个经过B编码的字符串，值可以是任何合法的B编码类型

# torrent文件结构

announce:Tracker的主服务器 announce-list：Tracker服务器列表 comment:种子文件的注释 comment.utf-8：种子文件注释的utf-8编码 creation
date：种子文件建立的时间，是从1970年1月1日00:00:00到现在的秒数。 encoding:种子文件的默认编码，比如GB2312，Big5，utf-8等
info：所有关于下载的文件的信息都在这个字段里，它包括多个子字段，而且根据下载的是单个文件还是多个文件，子字段的项目会不同，具体介绍在后面。 nodes:
最后的一个字段是nodes字段，这个字段包含一系列ip和相应端口的列表，是用于连接DHT初始node。

# 代码结构
[原项目地址](https://github.com/veggiedefender/torrent-client.git)
[博客](https://blog.jse.li/posts/torrent/)
[中文](https://mojotv.cn/go/golang-torrent)
torrentfile torrent文件解析

package p2p

import (
	"BTClient/client"
	"BTClient/message"
	"BTClient/peers"
	"bytes"
	"crypto/sha1"
	"fmt"
	"log"
	"runtime"
	"time"
)

// MaxBlockSize is the largest number of bytes a request can ask for
// 一个块 通常为16KB
const MaxBlockSize = 16384

// MaxBacklog is the number of unfulfilled requests a client can have in its pipeline
const MaxBacklog = 5

// Torrent holds data required to download a torrent from a list of peers
type Torrent struct {
	Peers       []peers.Peer
	PeerID      [20]byte
	InfoHash    [20]byte
	PieceHashes [][20]byte
	PieceLength int
	Length      int
	Name        string
	File        []FileInfo
}

type FileInfo struct {
	Path   string
	Length int
}

type pieceWork struct {
	index  int
	hash   [20]byte
	length int
}

type pieceResult struct {
	index int
	buf   []byte
}

type pieceProgress struct {
	index  int
	client *client.Client
	// 当前分片的 大小
	buf        []byte
	downloaded int
	requested  int
	backlog    int
}

// 读取响应
func (state *pieceProgress) readMessage() error {
	msg, err := state.client.Read() // this call blocks
	if err != nil {
		return err
	}

	if msg == nil { // keep-alive
		return nil
	}

	switch msg.ID {
	case message.MsgUnchoke:
		state.client.Choked = false
	case message.MsgChoke:
		state.client.Choked = true
	case message.MsgHave:
		index, err := message.ParseHave(msg)
		if err != nil {
			return err
		}
		state.client.Bitfield.SetPiece(index)
	case message.MsgPiece:
		// 返回 下载字节数
		n, err := message.ParsePiece(state.index, state.buf, msg)
		if err != nil {
			return err
		}
		// 下载的字节数
		state.downloaded += n
		// 任务完成
		state.backlog--
	}
	return nil
}

// 尝试下载分片
func attemptDownloadPiece(c *client.Client, pw *pieceWork) ([]byte, error) {
	// 进度
	state := pieceProgress{
		index:  pw.index,
		client: c,
		buf:    make([]byte, pw.length),
	}

	// Setting a deadline helps get unresponsive peers unstuck.
	// 30 seconds is more than enough time to download a 262 KB piece
	c.Conn.SetDeadline(time.Now().Add(30 * time.Second))
	defer c.Conn.SetDeadline(time.Time{}) // Disable the deadline

	// 已经下载的字节  当前分片的总长度
	for state.downloaded < pw.length {
		fmt.Println(state.index, state.downloaded)

		// If unchoked, send requests until we have enough unfulfilled requests
		if !state.client.Choked {
			// 节点准备好了
			// 保持一定数量的未完成请求.这样可以将连接的吞吐量提高一个数量级
			// 此部分是单线程 所以 backlog 只会有当前线程修改
			// 已经发送的 块 请求数量 == MaxBacklog && 已经发出 获取 分片总大小 == 当先分片大小，会读取剩余的请求
			for state.backlog < MaxBacklog && state.requested < pw.length {
				blockSize := MaxBlockSize
				// Last block might be shorter than the typical block
				// 最后一块可能比典型块短
				if pw.length-state.requested < blockSize {
					blockSize = pw.length - state.requested
				}

				// 发送需要下载的分片 index start end
				err := c.SendRequest(pw.index, state.requested, blockSize)
				if err != nil {
					return nil, err
				}
				state.backlog++
				state.requested += blockSize
			}
		}

		// 一直读取 会获取节点状态
		err := state.readMessage()
		if err != nil {
			return nil, err
		}
	}

	return state.buf, nil
}

func checkIntegrity(pw *pieceWork, buf []byte) error {
	hash := sha1.Sum(buf)
	if !bytes.Equal(hash[:], pw.hash[:]) {
		return fmt.Errorf("Index %d failed integrity check", pw.index)
	}
	return nil
}

func (t *Torrent) startDownloadWorker(peer peers.Peer, workQueue chan *pieceWork, results chan *pieceResult) {
	c, err := client.New(peer, t.PeerID, t.InfoHash)
	if err != nil {
		log.Printf("Could not handshake with %s. Disconnecting\n", peer.IP)
		return
	}
	defer c.Conn.Close()
	log.Printf("Completed handshake with %s\n", peer.IP)

	c.SendUnchoke()
	c.SendInterested()

	for pw := range workQueue {
		// bitfield 初始化状态是 255,255,255... 1111 1111
		// 255>>(7-0)&1 != 0
		// true -> 未下载
		if !c.Bitfield.HasPiece(pw.index) {
			// 当前 分片已经下载完成
			workQueue <- pw // Put piece back on the queue
			continue
		}

		// Download the piece
		buf, err := attemptDownloadPiece(c, pw)
		if err != nil {
			log.Println("Exiting", err)
			// 出错了，把任务放回队列
			workQueue <- pw // Put piece back on the queue
			return
		}

		// 校验 当前下载完成的分片
		err = checkIntegrity(pw, buf)
		if err != nil {
			log.Printf("Piece #%d failed integrity check\n", pw.index)
			workQueue <- pw // Put piece back on the queue
			continue
		}

		// 告知已经下载完成
		c.SendHave(pw.index)
		results <- &pieceResult{pw.index, buf}
	}
}

func (t *Torrent) calculateBoundsForPiece(index int) (begin int, end int) {
	begin = index * t.PieceLength
	end = begin + t.PieceLength
	if end > t.Length {
		end = t.Length
	}
	return begin, end
}

func (t *Torrent) calculatePieceSize(index int) int {
	begin, end := t.calculateBoundsForPiece(index)
	return end - begin
}

// Download downloads the torrent. This stores the entire file in memory.
func (t *Torrent) Download() ([]byte, error) {
	log.Println("Starting download for", t.Name)
	// Init queues for workers to retrieve work and send results
	// 初始化队列供工作人员检索工作并发送结果
	workQueue := make(chan *pieceWork, len(t.PieceHashes))
	results := make(chan *pieceResult)

	for index, hash := range t.PieceHashes {
		// 计算每个分片的大小 最后一片大小可能小于 PieceSize
		length := t.calculatePieceSize(index)
		// 放入 工作队列
		workQueue <- &pieceWork{index, hash, length}
	}

	// Start workers
	for _, peer := range t.Peers {
		go t.startDownloadWorker(peer, workQueue, results)
	}

	// Collect results into a buffer until full
	// 将结果收集到缓冲区中直至充满
	buf := make([]byte, t.Length)
	donePieces := 0
	for donePieces < len(t.PieceHashes) {
		res := <-results
		// 这一步提取 到 torrent 中
		begin, end := t.calculateBoundsForPiece(res.index)
		copy(buf[begin:end], res.buf)
		donePieces++

		percent := float64(donePieces) / float64(len(t.PieceHashes)) * 100
		numWorkers := runtime.NumGoroutine() - 1 // subtract 1 for main thread
		log.Printf("(%0.2f%%) Downloaded piece #%d from %d peers\n", percent, res.index, numWorkers)
	}
	close(workQueue)

	return buf, nil
}

package handshake

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	infoHash := [20]byte{64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134}
	peerID := [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
	h := New(infoHash, peerID)
	expected := &Handshake{
		Pstr:     "BitTorrent protocol",
		InfoHash: [20]byte{64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134},
		PeerID:   [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
	}
	assert.Equal(t, expected, h)
}

func TestSerialize(t *testing.T) {
	tests := map[string]struct {
		input  *Handshake
		output []byte
	}{
		"serialize message": {
			input: &Handshake{
				Pstr:     "BitTorrent protocol",
				InfoHash: [20]byte{64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134},
				PeerID:   [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
			},
			output: []byte{19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
		},
		"different pstr": {
			input: &Handshake{
				Pstr:     "BitTorrent protocol, but cooler?",
				InfoHash: [20]byte{64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134},
				PeerID:   [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
			},
			output: []byte{32, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 44, 32, 98, 117, 116, 32, 99, 111, 111, 108, 101, 114, 63, 0, 0, 0, 0, 0, 0, 0, 0, 64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
		},
	}

	for _, test := range tests {
		buf := test.input.Serialize()
		assert.Equal(t, test.output, buf)
	}
}

func TestRead(t *testing.T) {

	fmt.Println([]byte("195.26.197.26"))
	tests := map[string]struct {
		input  []byte
		output *Handshake
		fails  bool
	}{
		"parse handshake into struct": {
			input: []byte{19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
			output: &Handshake{
				Pstr:     "BitTorrent protocol",
				InfoHash: [20]byte{64, 96, 128, 218, 60, 212, 49, 237, 172, 197, 74, 255, 239, 98, 49, 153, 46, 240, 191, 134},
				PeerID:   [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
			},
			fails: false,
		},
		"empty": {
			input:  []byte{},
			output: nil,
			fails:  true,
		},
		"Not enough bytes": {
			input:  []byte{19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111},
			output: nil,
			fails:  true,
		},
		"pstrlen is 0": {
			input:  []byte{0, 0, 0},
			output: nil,
			fails:  true,
		},
	}

	for _, test := range tests {
		reader := bytes.NewReader(test.input)
		m, err := Read(reader)
		if test.fails {
			assert.NotNil(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, test.output, m)
	}
}

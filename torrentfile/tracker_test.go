package torrentfile

import (
	"BTClient/client"
	"BTClient/p2p"
	"BTClient/peers"
	"encoding/binary"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net"
	"net/url"
	"testing"
	"time"
)

func TestBuildTrackerURL(t *testing.T) {
	to := TorrentFile{
		Announce: "http://bttracker.debian.org:6969/announce",
		// all
		//InfoHash: [20]byte{85, 57, 15, 31, 20, 98, 244, 202, 73, 202, 4, 111, 119, 35, 156, 68, 240, 78, 88, 43},
		Torrent: p2p.Torrent{
			InfoHash: [20]byte{159, 41, 44, 147, 235, 13, 189, 215, 255, 122, 74, 165, 81, 170, 161, 234, 124, 175, 224, 4},
			Length:   353370112,
		},
	}

	peerID := [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
	const port uint16 = 6882

	url, err := to.buildTrackerURL(peerID, port)

	expected := "http://bttracker.debian.org:6969/announce?compact=1&downloaded=0&info_hash=%D8%F79%CE%C3%28%95l%CC%5B%BF%1F%86%D9%FD%CF%DB%A8%CE%B6&left=351272960&peer_id=%01%02%03%04%05%06%07%08%09%0A%0B%0C%0D%0E%0F%10%11%12%13%14&port=6881&uploaded=0"
	assert.Nil(t, err)
	assert.Equal(t, url, expected)
}

func TestRequestPeers(t *testing.T) {
	/*	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			response := []byte(
				"d" +
					"8:interval" + "i900e" +
					"5:peers" + "12:" +
					string([]byte{
						192, 0, 2, 123, 0x1A, 0xE1, // 0x1AE1 = 6881
						127, 0, 0, 1, 0x1A, 0xE9, // 0x1AE9 = 6889
					}) + "e")
			w.Write(response)
		}))
		defer ts.Close()*/
	tf := TorrentFile{
		Announce: "http://bttracker.debian.org:6969/announce",
		Torrent: p2p.Torrent{
			InfoHash: [20]byte{159, 41, 44, 147, 235, 13, 189, 215, 255, 122, 74, 165, 81, 170, 161, 234, 124, 175, 224, 4},
			Length:   353370112,
		},
	}

	peerID := [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
	const port uint16 = 6882
	// expected := []peers.Peer{
	//     {IP: net.IP{192, 0, 2, 123}, Port: 6881},
	// 	   {IP: net.IP{127, 0, 0, 1}, Port: 6889},
	// }

	err := tf.requestPeers(peerID, port)

	assert.Nil(t, err)

	fmt.Println(tf.Peers)
	assert.Equal(t, true, len(tf.Peers) > 0)
}

func TestGetPeersFromFile(t *testing.T) {
	//path := "/Users/exler/Desktop/bit.torrent"
	path := "../debian-10.9.0-amd64-netinst.iso.torrent"
	// http://bttracker.debian.org:6969/announce?compact=1&downloaded=0&info_hash=%9F%29%2C%93%EB%0D%BD%D7%FFzJ%A5Q%AA%A1%EA%7C%AF%E0%04&left=353370112&peer_id=%01%02%03%04%05%06%07%08%09%0A%0B%0C%0D%0E%0F%10%11%12%13%14&port=6881&uploaded=0
	// http://sukebei.tracker.wf:8888/announce?compact=1&downloaded=0&info_hash=%85%C1%3B%16.%F9%08%27%2A%8F%C8%D7%21%17%3Em%93%2B%C0%F1&left=0&peer_id=%01%02%03%04%05%06%07%08%09%0A%0B%0C%0D%0E%0F%10%11%12%13%14&port=6881&uploaded=0
	peerID := [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
	const port uint16 = 6882

	file, err := TorrentFactory(path)
	fmt.Println(file.InfoHash)
	fmt.Println(file.Announce)

	if err != nil {
		assert.Nil(t, err)
	}

	err = file.requestPeers(peerID, port)
	fmt.Println(file.Peers)

	tt := make(chan int, len(file.Peers))

	for _, v := range file.Peers {
		go func(_p peers.Peer) {
			client.New(_p, peerID, file.InfoHash)
			tt <- 1
		}(v)
	}
	counter := 0

	for counter < len(file.Peers) {
		counter += <-tt
	}
	//assert.Equal(t, true, len(p) > 0)

}
func TestDownload(t *testing.T) {
	//path := "../debian-10.9.0-amd64-netinst.iso.torrent"
	path := "C:\\Users\\Exler\\Desktop\\bit.torrent"
	//path:="C:\\Users\\Exler\\Desktop\\torrent\\ubuntu-21.04-live-server-amd64.iso.torrent"
	//path := "C:\\Users\\Exler\\Desktop\\torrent\\debian-10.9.0-amd64-netinst.iso.torrent"

	file, err := TorrentFactory(path)
	if err != nil {
		assert.Nil(t, err)
	}

	// todo bitcomet 可以选择 文件按分块大小对齐
	err = file.DownloadToFile(path)
	if err != nil {
		assert.Nil(t, err)
	}

}

func TestUDP(t *testing.T) {
	/*	to := TorrentFile{
		Announce: "udp://open.demonii.com:1337/announce",
		// all
		//InfoHash: [20]byte{85, 57, 15, 31, 20, 98, 244, 202, 73, 202, 4, 111, 119, 35, 156, 68, 240, 78, 88, 43},
		Torrent: p2p.Torrent{
			InfoHash: [20]byte{159, 41, 44, 147, 235, 13, 189, 215, 255, 122, 74, 165, 81, 170, 161, 234, 124, 175, 224, 4},
			Length:   353370112,
		},
	}*/
	//peerID := [20]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
	//const port uint16 = 6882
	//base, err := to.buildTrackerURL(peerID, port)

	urls := []string{
		"udp://p4p.arenabg.ch:1337/announce",
		"udp://tracker.opentrackr.org:1337/announce",
		"udp://tracker.openbittorrent.com:6969/announce",
		"udp://exodus.desync.com:6969/announce",
		"udp://www.torrent.eu.org:451/announce",
		"udp://tracker.torrent.eu.org:451/announce",
		"udp://retracker.lanta-net.ru:2710/announce",
		"udp://open.stealth.si:80/announce",
		"udp://zephir.monocul.us:6969/announce",
		"udp://wassermann.online:6969/announce",
		"udp://vibe.community:6969/announce",
		"udp://valakas.rollo.dnsabr.com:2710/announce",
		"udp://udp-tracker.shittyurl.org:6969/announce",
		"udp://u.wwwww.wtf:1/announce",
		"udp://tracker1.bt.moack.co.kr:80/announce",
		"udp://tracker0.ufibox.com:6969/announce",
		"udp://tracker.zerobytes.xyz:1337/announce",
		"udp://tracker.uw0.xyz:6969/announce",
		"udp://tracker.theoks.net:6969/announce",
		"udp://tracker.shkinev.me:6969/announce",
		"udp://tracker.ololosh.space:6969/announce",
		"udp://tracker.nrx.me:6969/announce",
		"udp://tracker.moeking.me:6969/announce",
		"udp://tracker.lelux.fi:6969/announce",
		"udp://tracker.ccp.ovh:6969/announce",
		"udp://tracker.breizh.pm:6969/announce",
		"udp://tracker.blacksparrowmedia.net:6969/announce",
		"udp://tracker.army:6969/announce",
		"udp://tracker.altrosky.nl:6969/announce",
		"udp://tracker.0x.tf:6969/announce",
		"udp://tracker-de.ololosh.space:6969/announce",
		"udp://torrentclub.online:54123/announce",
		"udp://t2.leech.ie:1337/announce",
		"udp://t1.leech.ie:1337/announce",
		"udp://retracker.netbynet.ru:2710/announce",
		"udp://public.publictracker.xyz:6969/announce",
		"udp://opentracker.i2p.rocks:6969/announce",
		"udp://opentor.org:2710/announce",
		"udp://open.publictracker.xyz:6969/announce",
		"udp://mts.tvbit.co:6969/announce",
		"udp://movies.zsw.ca:6969/announce",
		"udp://mail.realliferpg.de:6969/announce",
		"udp://ipv4.tracker.harry.lu:80/announce",
		"udp://inferno.demonoid.is:3391/announce",
		"udp://fe.dealclub.de:6969/announce",
		"udp://engplus.ru:6969/announce",
		"udp://edu.uifr.ru:6969/announce",
		"udp://drumkitx.com:6969/announce",
		"udp://discord.heihachi.pw:6969/announce",
		"udp://cutiegirl.ru:6969/announce",
		"udp://code2chicken.nl:6969/announce",
		"udp://camera.lei001.com:6969/announce",
		"udp://bubu.mapfactor.com:6969/announce",
		"udp://bt2.archive.org:6969/announce",
		"udp://bt1.archive.org:6969/announce",
		"udp://app.icon256.com:8000/announce",
		"udp://admin.videoenpoche.info:6969/announce",
		"udp://tsundere.pw:6969/announce",
		"udp://tracker4.itzmx.com:2710/announce",
		"udp://tracker2.dler.org:80/announce",
		"udp://tracker.monitorit4.me:6969/announce",
		"udp://tracker.loadbt.com:6969/announce",
		"udp://tracker.kali.org:6969/announce",
		"udp://tracker.filemail.com:6969/announce",
		"udp://tracker.dler.org:6969/announce",
		"udp://tr2.ysagin.top:2710/announce",
		"udp://tr.cili001.com:8070/announce",
		"udp://tr.bangumi.moe:6969/announce",
		"udp://t3.leech.ie:1337/announce",
		"udp://retracker.sevstar.net:2710/announce",
		"udp://public.tracker.vraphim.com:6969/announce",
		"udp://public-tracker.zooki.xyz:6969/announce",
		"udp://free.publictracker.xyz:6969/announce",
		"udp://concen.org:6969/announce",
		"udp://bt2.54new.com:8080/announce",
		"udp://bt.firebit.org:2710/announce",
		"udp://bclearning.top:6969/announce",
		"udp://anidex.moe:6969/announce",
	}

	th := make(chan int, len(urls))
	for _, v := range urls {
		go func(_url string) {
			defer func() { th <- 1 }()
			base, _ := url.Parse(_url)
			conn, err := net.Dial("udp", base.Host)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			defer conn.Close()
			conn.SetDeadline(time.Now().Add(5 * time.Second))

			//base, _ := url.Parse()
			//conn, err := net.Dial("udp", base.Hostname())
			//if err != nil {
			//	assert.Nil(t, err)
			//}

			payload := make([]byte, 16)
			// connection_id
			copy(payload[0:8], []byte{0, 1, 2, 3, 4, 5, 6, 7})
			// action 0
			binary.BigEndian.PutUint32(payload[8:12], uint32(0))
			// transaction_id
			copy(payload[12:16], []byte{0, 1, 2, 3})

			write, err := conn.Write(payload)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			fmt.Println(write)

			buf := []byte{}

			read, err := conn.Read(buf)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			fmt.Println(read)
			fmt.Println(buf[:read])
			fmt.Println(string(buf[:read]))
		}(v)
	}

	counter := 0
	for counter < len(urls) {
		<-th
		counter++
	}
	//conn.Write()
}

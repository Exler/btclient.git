package torrentfile

import (
	"BTClient/p2p"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

// TorrentFile torrent 文件读取后的元数据包装类
type TorrentFile struct {
	Announce     string
	AnnounceList []string
	p2p.Torrent
	// 多文件就是目录 单文件就是文件
	//Name        string
	//InfoHash    [20]byte
	//PieceHashes [][20]byte
	//PieceLength int
	//Length      int
}

func TorrentFactory(path string) (f TorrentFile, err error) {
	// 打开文件
	file, err := os.Open(path)
	if err != nil {
		return f, err
	}
	defer file.Close()

	all, err := ioutil.ReadAll(file)
	if err != nil {
		return f, err
	}

	var t torrent

	if strings.Contains(string(all), "5:files") {
		// 多个文件
		fmt.Println("检测到多个文件")
		t = &multipleTorrent{}
	} else {
		// 单个文件
		fmt.Println("检测到单个文件")
		t = &singleTorrent{}
	}

	err = t.fileParser(all)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	f, err = t.toTorrentFile()
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	fmt.Println(path, "info hash:", hex.EncodeToString(f.InfoHash[:]))

	return f, nil
}

// DownloadToFile 下载种子并将其写入文件
func (t *TorrentFile) DownloadToFile(path string) error {
	var peerID [20]byte
	_, err := rand.Read(peerID[:])
	if err != nil {
		return err
	}

	err = t.requestPeers(peerID, Port)
	if err != nil {
		return err
	}

	torrent := t.Torrent

	buf, err := torrent.Download()
	if err != nil {
		return err
	}

	outFile, err := os.Create(path)
	if err != nil {
		return err
	}
	defer outFile.Close()
	_, err = outFile.Write(buf)
	if err != nil {
		return err
	}
	return nil
}
